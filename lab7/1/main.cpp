#include <iostream>
#include <new>
#include <cstdlib>
#include <string>
#include <set>

// allocator.hpp

class Allocator
{
private:
    static int count;

    static void *malloc(size_t size);
    static void free(void *ptr) noexcept;

    friend void *operator new(size_t size);
    friend void operator delete(void *ptr) noexcept;

public:
    static void report();
};

// allocator.cpp

int Allocator::count = 0;

void *Allocator::malloc(size_t size)
{
    void *ptr = malloc(size);
    if (ptr == nullptr)
    {
        throw std::bad_alloc{};
    }
    ++count;
    std::cout << "allocated " << size << " bytes at " << ptr << std::endl;
    return ptr;
}

void Allocator::free(void *ptr) noexcept
{
    free(ptr);
    --count;
    std::cout << "freed memory at " << ptr << std::endl;
}

void Allocator::report()
{
    std::cout << "unfreed memory count is " << count << std::endl;
}

// main.cpp

void *operator new(size_t size)
{
    return Allocator::malloc(size);
}

void operator delete(void *ptr) noexcept
{
    return Allocator::free(ptr);
}

void operator delete(void *ptr, std::size_t) noexcept
{
    operator delete(ptr);
}

int main(int, char **)
{
    atexit(&Allocator::report);
    int *p = new int[5];
    delete p;
    std::string h = "helló világ alma körte barack";
    std::string *h2 = new std::string("helló világ alma körte barack");
    delete h2;
    // auto s = std::set{1, 2, 3, 4, 5};
    std::cout << "Hello, world!\n";
}
