#include <iostream>

class Noisy
{
public:
    explicit Noisy(int i) : i_{i}
    {
        std::cout << "Noisy{" << i << "} ctor\n";
        count++;
    }
    Noisy(Noisy const &o) : i_{o.i_}
    {
        std::cout << "Noisy copy ctor " << i_ << "\n";
        count++;
    }
    Noisy &operator=(Noisy const &o) = delete;
    ~Noisy()
    {
        std::cout << "Noisy dtor " << i_ << "\n";
        count--;
        i_ = rand(); /* ! */
    }
    static void report()
    {
        std::cout << count << " instance(s).\n";
    }

private:
    int i_;
    static int count;
};

int Noisy::count = 0;

template <typename T>
class Stack final
{
private:
    std::size_t size_ = 0;
    std::size_t capacity_;
    T *data_;

public:
    explicit Stack(std::size_t capacity)
        : capacity_{capacity},
          data_{static_cast<T *>(malloc(capacity * sizeof(T)))} {}

    Stack(const Stack &other)
        : capacity_{other.capacity_},
          data_{static_cast<T *>(malloc(other.capacity * sizeof(T)))}
    {
        for (int i = 0; i < other.size_; ++i)
        {
            push(other.data_[i]);
        }
    }

    Stack &operator=(const Stack &) = delete;

    ~Stack()
    {
        for (std::size_t i = 0; i < size_; ++i)
        {
            data_[i].~T();
        }
        free(data_);
    }

    void push(T t)
    {
        if (size_ == capacity_)
        {
            reserve(2 * capacity_);
        }
        new (&data_[size_++]) T(t);
    }

    T pop()
    {
        if (size_ == 0)
        {
            throw std::runtime_error("Stack is empty.");
        }
        size_--;
        T t = data_[size_];
        data_[size_].~T();
        if (size_ <= capacity_ / 4)
        {
            reserve(capacity_ / 2);
        }
        return t;
    }

    void reserve(std::size_t new_capacity)
    {
        if (new_capacity < size_)
        {
            throw std::runtime_error("Capacity must be at least size");
        }
        T *new_data = static_cast<T *>(malloc(new_capacity * sizeof(T)));
        for (std::size_t i = 0; i != size_; ++i)
        {
            new (&new_data[i]) T(data_[i]);
        }
        for (std::size_t i = 0; i != size_; ++i)
        {
            data_[i].~T();
        }
        free(data_);
        capacity_ = new_capacity;
        data_ = new_data;
    }
};

int main(int, char **)
{
    auto stack = Stack<Noisy>(10);
    std::cout << "################\n";
    stack.push(Noisy(1));
    std::cout << "################\n";
    stack.push(Noisy(2));
    std::cout << "################\n";
    auto noisy = stack.pop();
    std::cout << "################\n";
}
