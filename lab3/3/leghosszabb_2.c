#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

char *uj()
{
    char *str = (char *)malloc(1 * sizeof(char));
    str[0] = '\0';
    return str;
}

char *hozzafuz(char *str, char c)
{
    int len = strlen(str);
    len += 1;
    char *tmp = (char *)malloc((len + 1) * sizeof(char));
    strcpy(tmp, str);
    free(str);
    str = tmp;
    str[len - 1] = c;
    str[len] = '\0';
    return str;
}

void mozgat(char **hova, char **honnan)
{
    free(*hova);
    *hova = *honnan;
    *honnan = uj();
}

int main()
{
    char *leghosszabb = uj();
    char *szo = uj();
    int c;
    while ((c = getchar()) != EOF)
    {
        if (isalpha(c))
        {
            szo = hozzafuz(szo, c);
        }
        else
        {
            if (strlen(szo) > strlen(leghosszabb))
            {
                mozgat(&leghosszabb, &szo);
            }
            free(szo);
            szo = uj();
        }
    }
    printf("%s\n", leghosszabb);
    free(leghosszabb);
    free(szo);
    return 0;
}
