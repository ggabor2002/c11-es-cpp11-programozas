#include <iostream>

class base final
{
private:
    int n_;

public:
    base(int n) : n_{n} {}

    void print(std::ostream &os, int i) const
    {
        if (i == 0)
        {
            os << 0;
        }
        else if (i < 0)
        {
            os << '-';
            print_(os, -i);
        }
        else
        {
            print_(os, i);
        }
    }

private:
    void print_(std::ostream &os, int i) const
    {
        if (i == 0)
        {
            return;
        }
        print_(os, i / n_);
        char c = i % n_;
        if (c < 10)
        {
            c = '0' + c;
        }
        else
        {
            c = 'A' + c - 10;
        }
        os << c;
    }
};

class Printer
{
private:
    std::ostream &os;
    const base &base_;

public:
    Printer(std::ostream &os, const base &base) : os(os), base_{base} {}

    std::ostream &operator<<(int i) const
    {
        base_.print(os, i);
        return os;
    }
};

Printer operator<<(std::ostream &os, const base &base)
{
    return Printer{os, base};
}

base bin{2};

int main()
{
    std::cout << bin << 0 << std::endl;        /* 0 */
    std::cout << bin << 240 << std::endl;      /* 11110000 */
    std::cout << base{2} << 240 << std::endl;  /* 11110000 */
    std::cout << base{7} << 240 << std::endl;  /* 462 */
    std::cout << base{16} << 240 << std::endl; /* F0 */
}
