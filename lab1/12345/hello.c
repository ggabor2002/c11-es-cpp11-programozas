#include <stdio.h>
#include "hello.h"

Language lang = language_en;

void hello(int i)
{
    switch (lang)
    {
    case language_en:
        printf("Hello %d!\n", i);
        break;
    case language_hu:
        printf("Szia %d!\n", i);
        break;
    default:
        break;
    }
}
