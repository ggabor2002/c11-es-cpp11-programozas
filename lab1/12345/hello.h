#ifndef HELLO_H_INCLUDED
#define HELLO_H_INCLUDED

typedef enum Language
{
    language_en,
    language_hu
} Language;

extern Language lang;

void hello(int i);

#endif // HELLO_H_INCLUDED
