#ifndef HELLO_H_INCLUDED
#define HELLO_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

void hello_c(void);

#ifdef __cplusplus
}
#endif

#endif // HELLO_H_INCLUDED
