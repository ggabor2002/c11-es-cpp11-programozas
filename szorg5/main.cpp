#include <iostream>

static int seged_1(int n, char honnan, char seged, char hova, int lepes_1)
{
    if (n == 0)
    {
        return lepes_1;
    }
    lepes_1 = seged_1(n - 1, honnan, hova, seged, lepes_1);
    std::cout << lepes_1++ << ". lepes: rakj 1-et: " << honnan << "->" << hova << std::endl;
    return seged_1(n - 1, seged, honnan, hova, lepes_1);
}

void hanoi_1(int n, char honnan, char seged, char hova)
{
    seged_1(n, honnan, seged, hova, 1);
}

static void seged_2(int n, char honnan, char seged, char hova, int &lepes_2)
{
    if (n == 0)
    {
        return;
    }
    seged_2(n - 1, honnan, hova, seged, lepes_2);
    std::cout << lepes_2++ << ". lepes: rakj 1-et: " << honnan << "->" << hova << std::endl;
    seged_2(n - 1, seged, honnan, hova, lepes_2);
}

void hanoi_2(int n, char honnan, char seged, char hova)
{
    int lepes_2 = 1;
    seged_2(n, honnan, seged, hova, lepes_2);
}

static int lepes_3;

static void seged_3(int n, char honnan, char seged, char hova)
{
    if (n == 0)
    {
        return;
    }
    seged_3(n - 1, honnan, hova, seged);
    std::cout << lepes_3++ << ". lepes: rakj 1-et: " << honnan << "->" << hova << std::endl;
    seged_3(n - 1, seged, honnan, hova);
}

void hanoi_3(int n, char honnan, char seged, char hova)
{
    lepes_3 = 1;
    seged_3(n, honnan, seged, hova);
}

int main()
{
    hanoi_1(3, 'A', 'B', 'C');
    std::cout << std::endl;
    hanoi_2(3, 'A', 'B', 'C');
    std::cout << std::endl;
    hanoi_3(3, 'A', 'B', 'C');
    return 0;
}
