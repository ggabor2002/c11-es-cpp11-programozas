#include <stack>
#include <string>
#include <iostream>
#include <functional>

using operation_t = std::function<double(double, double)>;

operation_t addition = [](double x, double y)
{
    return x + y;
};

operation_t subtraction = [](double x, double y)
{
    return x - y;
};

operation_t multiplication = [](double x, double y)
{
    return x * y;
};

operation_t division = [](double x, double y)
{
    return x / y;
};

int main()
{
    auto stack = std::stack<double>{};
    auto word = std::string{};
    while (std::cin >> word)
    {
        operation_t *operation;
        switch (word[0])
        {
        case '+':
            operation = &addition;
            break;
        case '-':
            operation = &subtraction;
            break;
        case '*':
            operation = &multiplication;
            break;
        case '/':
            operation = &division;
            break;
        default:
            operation = nullptr;
            break;
        }
        if (operation != nullptr)
        {
            auto y = stack.top();
            stack.pop();
            auto x = stack.top();
            stack.pop();
            auto res = std::invoke(*operation, x, y);
            stack.push(res);
        }
        else
        {
            auto d = std::stod(word);
            stack.push(d);
        }
    }
    std::cout << stack.top() << std::endl;
    stack.pop();
}
