#include <iostream>

int powint_v1(int base, unsigned exp)
{
    return exp == 0 ? 1 : base * powint_v1(base, exp - 1);
}

int powint_v2(int base, unsigned exp, int acc = 1)
{
    return exp == 0 ? acc : powint_v2(base, exp - 1, base * acc);
}

template <int base, int exp, int acc = 1>
struct PowInt_v3 : PowInt_v3<base, exp - 1, base * acc>
{
};

template <int base, int acc>
struct PowInt_v3<base, 0, acc>
{
    static constexpr int value = acc;
};

int gcd_v1(int a, int b)
{
    while (b > 0)
    {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}

int gcd_v2(int a, int b)
{
    return b == 0 ? a : gcd_v2(b, a % b);
}

template <int a, int b>
struct gcd_v3 : gcd_v3<b, a % b>
{
};

template <int a>
struct gcd_v3<a, 0>
{
    static constexpr int value = a;
};

int main(int, char **)
{
    std::cout << powint_v1(2, 10) << std::endl;
    std::cout << powint_v2(2, 10) << std::endl;
    std::cout << PowInt_v3<2, 10>::value << std::endl;

    std::cout << gcd_v1(8, 12) << std::endl;
    std::cout << gcd_v2(8, 12) << std::endl;
    std::cout << gcd_v3<8, 12>::value << std::endl;
}
