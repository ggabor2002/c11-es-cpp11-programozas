#!/bin/bash

rm -rf build
clear
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++ -Bbuild
cmake --build build --config Debug --target lab12_2 --
build/lab12_2
