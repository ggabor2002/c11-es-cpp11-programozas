cmake_minimum_required(VERSION 3.0.0)
project(lab12_4 VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 23)

add_executable(lab12_4 main.cpp)

target_compile_options(lab12_4 PRIVATE -Werror -Wall -Wextra)
