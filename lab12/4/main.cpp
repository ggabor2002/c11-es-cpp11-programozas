#include <functional>
#include <map>
#include <tuple>
#include <iostream>

/*
Mit csinál az alábbi függvény?

Cache-el.

Az általa visszaadott függvény, amely hívásmódjában kompatibilis az első
paramétereként megadott függvénnyel (ugyanolyan paraméterek és visszatérési
érték), miben viselkedik másképpen?

Igen, kompatibilis.
Lehet, hogy a cache-ből veszi az értéket, ha a függvénynek lenne mellékhatása,
akkor ebben az esetben nem lesz.
*/

template <typename RET, typename... ARGS>
// A paraméter miért nem std::function?
auto memoized_func(RET (*func)(ARGS...)) -> std::function<RET(ARGS...)>
{
    std::map<std::tuple<ARGS...>, RET> retvals;
    auto memoized = [=](ARGS... args) mutable -> RET
    {
        auto args_tuple = std::make_tuple(args...);
        auto found = retvals.find(args_tuple); // Egy if-be rakott init-statement nem lenne szebb?
        if (found != retvals.end())
            return found->second;
        auto retval = func(args...); // std::invoke nem lenne szebb?
        retvals.insert(make_pair(args_tuple, retval));
        return retval;
    };
    return memoized;
}

using llu = long long unsigned;

// Mikor érdemes ilyet használni?
// Ha valamit költséges kiszámolni.
llu fib(llu n)
{
    if (n < 2)
        return n;
    return fib(n - 1) + fib(n - 2);
}

// Mikor nem szabad ilyet használni?
// Ha nem matematikailag tiszta függvényt adunk át neki. Azaz nem determinisztikus vagy mellékhatása van.

// Írj kódot a működés bemutatására!
auto cached_fib = memoized_func(fib);

int main(int, char **)
{
    std::cout << cached_fib(40) << std::endl;
}
