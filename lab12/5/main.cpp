#include <iostream>

template <int...>
struct seq
{
};

template <int N, int... S>
struct gens : gens<N - 1, N - 1, S...>
{
};

template <int... S>
struct gens<0, S...>
{
    using type = seq<S...>;
};

int main(int, char **)
{
    gens<4>::type g4;
}
