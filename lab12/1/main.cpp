#include <iostream>

using llu = long long unsigned;

// Fibonacci, mint régen
auto fib(llu N)
{
    if (N < 2)
        return N;
    return fib(N - 1) + fib(N - 2);
}

// Van ötleted, hogyan lehetne gyorsítani?
auto _fib2(llu n1, llu n2, llu N)
{
    if (N == 0)
        return n1;
    return _fib2(n2, n1 + n2, N - 1);
}
auto fib2(llu N)
{
    return _fib2(0, 1, N);
}

// Fibonacci, sablon metaprogramozással I.
template <llu N>
struct Fib
{
    static constexpr llu value = Fib<N - 1>::value + Fib<N - 2>::value;
};

template <>
struct Fib<0>
{
    static constexpr llu value = 0;
};

template <>
struct Fib<1>
{
    static constexpr llu value = 1;
};

// Fibonacci, sablon metaprogramozással II.
template <llu i>
auto print_fib_from()
{
    std::cout << Fib<i>::value << std::endl;
    print_fib_from<i - 1>();
}

template <>
auto print_fib_from<0>()
{
}

int main(int, char **)
{
    std::cout << Fib<40>::value << std::endl;

    print_fib_from<40>();
}
