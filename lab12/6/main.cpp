#include <tuple>
#include <iostream>

template <int...>
struct seq
{
};
template <int N, int... S>
struct gens : gens<N - 1, N - 1, S...>
{
};
template <int... S>
struct gens<0, S...>
{
    using type = seq<S...>;
};

double foo(int x, float y, double z)
{
    return x + y + z;
}

template <typename Ret, typename... Args>
struct save_it_for_later
{
    Ret (*func)(Args...);
    std::tuple<Args...> params;

    template <int... S>
    Ret callFunc(seq<S...>)
    {
        return func(std::get<S>(params)...);
    }

    Ret delayed_dispatch()
    {
        return callFunc(typename gens<sizeof...(Args)>::type{});
    }
};

int main(void)
{
    std::tuple<int, float, double> t = std::make_tuple(1, 1.2, 5);
    save_it_for_later<double, int, float, double> saved = {foo, t};

    std::cout << saved.delayed_dispatch() << std::endl;
}
