#!/bin/bash

clear
/usr/bin/cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++ -Bbuild && \
/usr/bin/cmake --build build --config Debug --target lab11_4 -- && \
build/lab11_4
