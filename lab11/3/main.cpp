#include <iostream>

template <typename BASE, typename DERIVED>
struct IsDerived
{
private:
    static constexpr bool get_value(BASE *)
    {
        return true;
    }

    static constexpr bool get_value(void *)
    {
        return false;
    }

public:
    static constexpr bool value = get_value(static_cast<DERIVED *>(nullptr));
};

class Base
{
};

class Derived : public Base
{
};

int main()
{
    std::cout << std::boolalpha << IsDerived<Base, Derived>::value << std::endl;      /* true */
    std::cout << std::boolalpha << IsDerived<Base, std::ostream>::value << std::endl; /* false */
}
