#pragma once

#include <iostream>
#include <set>
#include <compare>

class Noisy
{
private:
    int data;

    static std::set<Noisy const *> constructed;
    static std::set<Noisy const *> valid;
    static bool is_valid(Noisy const *n);
    static bool is_constructed(Noisy const *n);

public:
    static bool noisy;
    static void report();

    int get_data() const;

    // Noisy(int i = 0);
    Noisy(int i);
    ~Noisy() noexcept(false);

    // Noisy(Noisy const &o);
    Noisy(Noisy const &o) = delete;
    Noisy(Noisy &&o);

    // Noisy &operator=(Noisy const &o);
    Noisy &operator=(Noisy const &o) = delete;
    Noisy &operator=(Noisy &&o);

    auto operator<=>(const Noisy &) const = default;

    static size_t get_constructed_count();
    static size_t get_valid_count();

    friend std::ostream &operator<<(std::ostream &os, Noisy const &n);
    friend std::istream &operator>>(std::istream &is, Noisy &n);
};

std::ostream &operator<<(std::ostream &os, Noisy const &n);
std::istream &operator>>(std::istream &is, Noisy &n);
