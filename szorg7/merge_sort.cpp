#include "merge_sort.hpp"
#include "supernoisy.hpp"

template <typename T>
static void merge(T *in, int begin, int mid, int end, T *out)
{
    int i = begin, j = mid;
    for (int c = begin; c < end; ++c)
    {
        if (i < mid && (j >= end || in[i] <= in[j]))
        {
            new (&out[c]) T(std::move(in[i]));
            i++;
        }
        else
        {
            new (&out[c]) T(std::move(in[j]));
            j++;
        }
    }
}

template <typename T>
static void copy(T *in, int begin, int end, T *out)
{
    for (int c = begin; c < end; ++c)
    {
        out[c] = std::move(in[c]);
        in[c].~T();
    }
}

template <typename T>
static void merge_sort_(T *tomb, int begin, int end, T *temp)
{
    if (end - begin < 2)
        return;
    int mid = (begin + end) / 2;
    merge_sort_(tomb, begin, mid, temp);
    merge_sort_(tomb, mid, end, temp);
    merge(tomb, begin, mid, end, temp);
    copy(temp, begin, end, tomb);
}

template <typename T>
void merge_sort(T *tomb, int begin, int end)
{
    T *tmp = static_cast<T *>(::operator new[]((end - begin) * sizeof(T)));
    merge_sort_(tomb + begin, 0, end - begin, tmp);
    ::operator delete[](tmp);
}

template void merge_sort(Noisy *, int, int);
