#include <iostream>
#include "merge_sort.hpp"
#include "supernoisy.hpp"

int main(int, char **)
{
    Noisy tomb[10] = {4, 1, 10, 3, 9, 6, 5, 8, 7, 2};
    std::cout << "################\n";
    merge_sort(tomb, 0, 10);
    std::cout << "################\n";
    for (const auto &n : tomb)
    {
        std::cout << n << std::endl;
    }
    std::cout << "################\n";
}
