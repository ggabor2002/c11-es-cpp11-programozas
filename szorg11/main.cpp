#include <iostream>
#include <type_traits>

template <typename T>
struct IsCharacter
{
    static constexpr bool value = false;
};

template <>
struct IsCharacter<char>
{
    static constexpr bool value = true;
};

template <>
struct IsCharacter<unsigned char>
{
    static constexpr bool value = true;
};

template <>
struct IsCharacter<signed char>
{
    static constexpr bool value = true;
};

template <>
struct IsCharacter<char16_t>
{
    static constexpr bool value = true;
};

template <>
struct IsCharacter<char32_t>
{
    static constexpr bool value = true;
};

template <typename T>
inline constexpr bool IsCharacterV = IsCharacter<std::remove_const_t<T>>::value;

template <typename T>
struct IsString
{
    static constexpr bool value = std::is_pointer_v<T> && IsCharacterV<std::remove_pointer_t<T>>;
};

template <>
struct IsString<std::string>
{
    static constexpr bool value = true;
};

template <typename T>
inline constexpr bool IsStringV = IsString<std::remove_const_t<T>>::value;

template <typename T>
void print(T what, typename std::enable_if_t<!IsCharacterV<T> && std::is_arithmetic_v<T>> * = nullptr)
{
    std::cout << what << std::endl;
}

template <typename T>
void print(T what, typename std::enable_if_t<IsCharacterV<T>> * = nullptr)
{
    std::cout << '\'' << what << '\'' << std::endl;
}

template <typename T>
void print(T what, typename std::enable_if_t<IsStringV<T>> * = nullptr)
{
    std::cout << '\"' << what << '\"' << std::endl;
}

int main(int, char **)
{
    print(5);
    print(5.1);
    print('a');
    print("hello");
}
