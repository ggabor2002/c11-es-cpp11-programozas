#include <iostream>

class String
{
private:
    char *data;

public:
    String(String &&) noexcept;
    // Nincsen destruktor
};

String::String(String &&the_other) noexcept
{
    // Szerintem kéne működnie, csak az std::move szükségtelen.
    data = std::move(the_other.data);
    the_other.data = nullptr;
}

int main(int, char **)
{
    std::cout << "Hello, world!\n";
}
