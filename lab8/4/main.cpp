#include <stdexcept>
#include <iostream>
#include <algorithm>

class Matrix final
{
private:
    size_t w_;
    size_t h_;
    double *data_;

public:
    size_t get_w() const
    {
        return w_;
    }

    size_t get_h() const
    {
        return h_;
    }

    explicit Matrix()
        : w_{0},
          h_{0},
          data_{nullptr}
    {
    }

    explicit Matrix(size_t w, size_t h)
        : w_{w},
          h_{h},
          data_{new double[w * h]{}}
    {
    }

    explicit Matrix(size_t w, size_t h, std::initializer_list<double> data)
        : w_{w},
          h_{h},
          data_{new double[w * h]}
    {
        if (data.size() != w_ * h_)
        {
            throw std::length_error{"explicit Matrix(size_t w, size_t h, std::initializer_list<double> data)"};
        }
        std::copy(data.begin(), data.end(), data_);
    }

    ~Matrix()
    {
        delete[] data_;
    }

    Matrix(const Matrix &other)
        : w_{other.w_},
          h_{other.h_},
          data_{new double[other.w_ * other.h_]}
    {
        std::copy(other.data_, other.data_ + w_ * h_, data_);
    }

    Matrix(Matrix &&other) noexcept
        : w_{other.w_},
          h_{other.h_},
          data_{other.data_}
    {
        other.w_ = 0;
        other.h_ = 0;
        other.data_ = nullptr;
    }

    Matrix &operator=(Matrix other) noexcept
    {
        swap(other);
        return *this;
    }

    void swap(Matrix &other) noexcept
    {
        std::swap(w_, other.w_);
        std::swap(h_, other.h_);
        std::swap(data_, other.data_);
    }

    double &operator()(size_t i, size_t j)
    {
        if (i < 1 || i > w_ || j < 1 || j > h_)
        {
            throw std::out_of_range{"double &operator()(size_t i, size_t j)"};
        }
        return data_[(j - 1) * w_ + (i - 1)];
    }

    const double &operator()(size_t i, size_t j) const
    {
        if (i < 1 || i > w_ || j < 1 || j > h_)
        {
            throw std::out_of_range{"const double &operator()(size_t i, size_t j) const"};
        }
        return data_[(j - 1) * w_ + (i - 1)];
    }

    Matrix operator+(const Matrix &other) const &
    {
        if (w_ != other.w_ || h_ != other.h_)
        {
            throw std::invalid_argument{"Matrix operator+(const Matrix &other) const &"};
        }
        Matrix result{w_, h_};
        for (size_t i = 0; i < w_ * h_; ++i)
        {
            result.data_[i] = data_[i] + other.data_[i];
        }
        return result;
    }

    Matrix operator+(const Matrix &other) &&
    {
        if (w_ != other.w_ || h_ != other.h_)
        {
            throw std::invalid_argument{"Matrix operator+(const Matrix &other) &&"};
        }
        for (size_t i = 0; i < w_ * h_; ++i)
        {
            data_[i] += other.data_[i];
        }
        return std::move(*this);
    }

    Matrix operator+(Matrix &&other) const
    {
        return std::move(other) + *this;
    }
};

std::ostream &operator<<(std::ostream &os, const Matrix &matrix)
{
    for (size_t j = 1; j <= matrix.get_h(); ++j)
    {
        for (size_t i = 1; i <= matrix.get_w(); ++i)
        {
            os << matrix(i, j) << " ";
        }
        os << std::endl;
    }
    return os;
}

int main(int, char **)
{

    Matrix m{3, 3};
    m(1, 1) = 9;
    m(2, 3) = 5;
    std::cout << m;

    Matrix a{3, 3};
    a(1, 1) = 9;
    a(2, 3) = 5;
    Matrix b{3, 3};
    b(1, 1) = 9;
    b(2, 3) = 5;
    Matrix c = a + b;
    std::cout << c;

    Matrix m2{3, 3, {9, 0, 0, 0, 0, 0, 0, 5, 0}};
    std::cout << m2;
}
