#include <cstdio>
#include <string>
#include <stdexcept>
#include <utility>
#include <vector>

/* FILE* RAII */
class FilePtr
{
public:
    explicit FilePtr(FILE *fp = nullptr) : fp_{fp} {}
    explicit FilePtr(std::string filename, const char *modes)
        : fp_{fopen(filename.c_str(), modes)},
          filename_{std::move(filename)}
    {
        if (fp_ == nullptr)
        {
            throw std::runtime_error{"Could not open file."};
        }
    }
    ~FilePtr()
    {
        close_if_open();
    }
    FilePtr &operator=(FILE *fp)
    {
        close_if_open();
        fp_ = fp;
        return *this;
    }
    operator FILE *() const
    {
        return fp_;
    }

    void swap(FilePtr &other) noexcept
    {
        std::swap(fp_, other.fp_);
        std::swap(filename_, other.filename_);
    }

    /* nem másolható */
    FilePtr(FilePtr const &) = delete;
    FilePtr(FilePtr &&other) noexcept
        : fp_{other.fp_},
          filename_{std::move(other.filename_)}
    {
        other.fp_ = nullptr;
    }

    FilePtr &operator=(FilePtr other) noexcept
    {
        swap(other);
        return *this;
    }

private:
    FILE *fp_;
    std::string filename_;

    void close_if_open()
    {
        if (fp_ != nullptr)
            fclose(fp_);
        fp_ = nullptr;
    }
};

FilePtr open_for_writing(char const *name)
{
    FilePtr fp{name, "wt"};
    return fp;
}

int main()
{
    FilePtr fp;
    fp = open_for_writing("hello.txt");
    fprintf(fp, "Hello vilag");
    fp = open_for_writing("hello2.txt");
    fprintf(fp, "Hello vilag");

    std::vector<FilePtr> files;
    files.push_back(open_for_writing("hello.txt"));
    files.push_back(open_for_writing("hello2.txt"));
    fprintf(files[0], "hello.txt");
    fprintf(files[1], "hello.txt");
}
