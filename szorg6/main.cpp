#include <iostream>
#include <utility>  // std::forward
#include <optional> // std::bad_optional_access

class Noisy
{
public:
    Noisy() : i_{0}
    {
        std::cout << "Noisy default ctor\n";
        count++;
    }
    explicit Noisy(int i) : i_{i}
    {
        std::cout << "Noisy{" << i << "} ctor\n";
        count++;
    }
    Noisy(Noisy const &o) : i_{o.i_}
    {
        std::cout << "Noisy copy ctor " << i_ << "\n";
        count++;
    }
    Noisy(Noisy &&o) noexcept : i_{o.i_}
    {
        std::cout << "Noisy move ctor " << i_ << "\n";
        count++;
    }
    Noisy &operator=(Noisy const &o)
    {
        i_ = o.i_;
        std::cout << "Noisy copy assignment " << i_ << "\n";
        return *this;
    }
    Noisy &operator=(Noisy &&o) noexcept
    {
        i_ = o.i_;
        std::cout << "Noisy move assignment " << i_ << "\n";
        return *this;
    }
    ~Noisy()
    {
        std::cout << "Noisy dtor " << i_ << "\n";
        count--;
    }
    static void report()
    {
        std::cout << count << " instance(s).\n";
    }

private:
    int i_;
    static int count;
};
int Noisy::count = 0;

template <typename T>
class Optional
{
private:
    bool has_value_ = false;
    alignas(T) char value_[sizeof(T)];

public:
    constexpr Optional() = default;
    template <typename... Args>
    constexpr Optional(Args &&...args) : has_value_{true}
    {
        new (value_) T{std::forward<Args>(args)...};
    }
    constexpr Optional(const T &value) : has_value_{true}
    {
        new (value_) T{value};
    }
    constexpr Optional(const Optional &other) : has_value_{other.has_value_}
    {
        if (other.has_value_)
        {
            new (value_) T{other.value()};
        }
    }
    // error: ‘constexpr’ destructors only available with ‘-std=c++20’ or ‘-std=gnu++20’
    ~Optional()
    {
        if (has_value_)
        {
            value().~T();
        }
    }
    template <typename... Args>
    constexpr Optional &operator=(Args &&...args)
    {
        if (has_value_)
        {
            value() = T{args...};
        }
        else
        {
            new (value_) T{std::forward<Args>(args)...};
        }
        has_value_ = true;
        return *this;
    }
    constexpr Optional &operator=(const T &value)
    {
        if (has_value_)
        {
            value() = T{value};
        }
        else
        {
            new (value_) T{value};
        }
        has_value_ = true;
        return *this;
    }
    constexpr Optional &operator=(const Optional &other)
    {
        if (has_value_)
        {
            if (other.has_value_)
            {
                value() = other.value();
            }
            else
            {
                value().~T();
                has_value_ = false;
            }
        }
        else
        {
            if (other.has_value_)
            {
                new (value_) T{other.value()};
                has_value_ = true;
            }
            else
            {
                // Semmi.
            }
        }
    }
    constexpr bool has_value() const
    {
        return has_value_;
    }
    constexpr explicit operator bool() const
    {
        return has_value();
    }
    constexpr T &value()
    {
        if (!has_value_)
        {
            throw std::bad_optional_access{};
        }
        return *reinterpret_cast<T *>(value_);
    }
    constexpr T &operator*()
    {
        return value();
    }
    constexpr const T &operator*() const
    {
        return value();
    }
    constexpr T *operator->()
    {
        if (!has_value_)
        {
            throw std::bad_optional_access{};
        }
        return &value();
    }
    constexpr const T *operator->() const
    {
        if (!has_value_)
        {
            throw std::bad_optional_access{};
        }
        return &value();
    }
};

int main(int, char **)
{
    Optional<Noisy> maybe_noisy;
    if (maybe_noisy)
    {
        std::cout << "Baj van!" << std::endl;
    }
    maybe_noisy = 911;
    if (maybe_noisy)
    {
        std::cout << "Minden jó." << std::endl;
    }
    return 0;
}
