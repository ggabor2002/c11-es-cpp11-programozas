#include <iostream>
#include <string>
#include <sstream>

static constexpr int gcd(int a, int b)
{
    int t = 0;
    while (b != 0)
    {
        t = a % b, a = b, b = t;
    }
    return a;
}

class Ratio final
{
public:
    explicit constexpr Ratio(int num = 0, int den = 1);
    constexpr int num() const { return num_; }
    constexpr int den() const { return den_; }
    constexpr operator double() const;

private:
    int num_;
    int den_;
};

constexpr Ratio::Ratio(int num, int den)
    : num_{num / gcd(num, den)},
      den_{den / gcd(num, den)}
{
}

constexpr Ratio::operator double() const
{
    return static_cast<double>(num_) / den_;
}

constexpr Ratio operator+(const Ratio &r)
{
    return r;
}

constexpr Ratio operator+(const Ratio &r1, const Ratio &r2)
{
    return Ratio{r1.num() * r2.den() + r2.num() * r1.den(), r1.den() * r2.den()};
}

constexpr Ratio &operator+=(Ratio &r1, const Ratio &r2)
{
    return r1 = r1 + r2;
}

constexpr Ratio operator-(const Ratio &r)
{
    return Ratio{-r.num(), r.den()};
}

constexpr Ratio operator-(const Ratio &r1, const Ratio &r2)
{
    return Ratio{r1.num() * r2.den() - r2.num() * r1.den(), r1.den() * r2.den()};
}

constexpr Ratio operator-=(Ratio &r1, const Ratio &r2)
{
    return r1 = r1 - r2;
}

constexpr Ratio operator*(const Ratio &r1, const Ratio &r2)
{
    return Ratio{r1.num() * r2.num(), r1.den() * r2.den()};
}

constexpr Ratio &operator*=(Ratio &r1, const Ratio &r2)
{
    return r1 = r1 * r2;
}

constexpr Ratio operator/(const Ratio &r1, const Ratio &r2)
{
    return Ratio{r1.num() * r2.den(), r1.den() * r2.num()};
}

constexpr Ratio operator/=(Ratio &r1, const Ratio &r2)
{
    return r1 = r1 / r2;
}

std::ostream &operator<<(std::ostream &os, const Ratio &r)
{
    os << r.num() << '/' << r.den();
    return os;
}

std::istream &operator>>(std::istream &is, Ratio &r)
{
    char c;
    int num, den;
    is >> num >> c >> den;
    r = Ratio{num, den};
    return is;
}

int main()
{
    Ratio r1{1, 10};
    Ratio r2{5, 10};
    Ratio r3 = r1 + r2;
    std::cout << r1 << '+' << r2 << '=' << r3 << std::endl;
    std::cin >> r3;
    std::cout << r3 << std::endl;
}
