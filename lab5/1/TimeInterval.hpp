#pragma once

#include <iostream>

class TimeInterval
{
public:
    using llu = long long unsigned;

private:
    llu time_in_minutes{};

public:
    TimeInterval();
    explicit TimeInterval(llu time_in_minutes);
    llu get_time_in_minutes() const;
};

std::ostream &operator<<(std::ostream &os, const TimeInterval &timeInterval);
TimeInterval operator+(const TimeInterval &timeInterval1, const TimeInterval &timeInterval2);

TimeInterval operator"" _h(TimeInterval::llu time_in_hours);
TimeInterval operator"" _m(TimeInterval::llu time_in_minutes);
