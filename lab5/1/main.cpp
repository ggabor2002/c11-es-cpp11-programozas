#include <iostream>

#include "TimeInterval.hpp"

int main(int, char **)
{
    TimeInterval i1{65};
    std::cout << i1 << std::endl; // 1h 5m

    std::cout << (5_h + 79_m) << std::endl; // 6h 19m
}
