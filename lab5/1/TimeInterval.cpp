#include "TimeInterval.hpp"

#include <iostream>

TimeInterval::TimeInterval(){};

TimeInterval::TimeInterval(llu time_in_minutes) : time_in_minutes{time_in_minutes} {};

TimeInterval::llu TimeInterval::get_time_in_minutes() const
{
    return time_in_minutes;
}

std::ostream &operator<<(std::ostream &os, const TimeInterval &timeInterval)
{
    TimeInterval::llu time_in_minutes = timeInterval.get_time_in_minutes();
    return os << time_in_minutes / 60 << "h " << time_in_minutes % 60 << "m";
}

TimeInterval operator+(const TimeInterval &timeInterval1, const TimeInterval &timeInterval2)
{
    return TimeInterval{timeInterval1.get_time_in_minutes() + timeInterval2.get_time_in_minutes()};
}

TimeInterval operator"" _h(TimeInterval::llu time_in_hours)
{
    return TimeInterval{time_in_hours * 60};
}

TimeInterval operator"" _m(TimeInterval::llu time_in_minutes)
{
    return TimeInterval{time_in_minutes};
}
