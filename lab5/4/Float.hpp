#pragma once

#include <iostream>

class Float
{
public:
    constexpr static float e = 0.0001;

private:
    float f{};

public:
    constexpr Float(float f) : f{f} {}

    explicit constexpr operator float()
    {
        return f;
    }
};

constexpr bool operator<(Float f1, Float f2)
{
    return static_cast<float>(f1) + Float::e < static_cast<float>(f2);
}

constexpr Float operator+(Float f1, Float f2)
{
    return static_cast<float>(f1) + static_cast<float>(f2);
}

constexpr Float operator-(Float f1, Float f2)
{
    return static_cast<float>(f1) - static_cast<float>(f2);
}

constexpr Float &operator+=(Float &f1, Float f2)
{
    return f1 = f1 + f2;
}

constexpr Float &operator-=(Float &f1, Float f2)
{
    return f1 = f1 - f2;
}

std::ostream &operator<<(std::ostream &os, Float f)
{
    return os << static_cast<float>(f);
}

constexpr bool operator>(Float f1, Float f2)
{
    return f2 < f1;
}

constexpr bool operator<=(Float f1, Float f2)
{
    return !(f2 < f1);
}

constexpr bool operator>=(Float f1, Float f2)
{
    return !(f1 < f2);
}

constexpr bool operator==(Float f1, Float f2)
{
    return !(f1 < f2) && !(f2 < f1);
}

constexpr bool operator!=(Float f1, Float f2)
{
    return f1 < f2 || f2 < f1;
}

constexpr Float operator+(Float f)
{
    return f;
}

constexpr Float operator-(Float f)
{
    return 0 - f;
}
