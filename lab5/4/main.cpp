#include <iostream>

#include "Float.hpp"

int main(int, char **)
{
    Float f1 = 1.0f,
          f2 = 1.00001f,
          f3 = 100;
    std::cout << (f1 < f2) << std::endl; /* hamis */
    std::cout << (f1 < f3) << std::endl; /* igaz */

    f1 = f2 + f3;
    for (Float f = 0.999; f < 1.001; f += 0.0001)
    {
        std::cout << f << '\t' << (f < 1.0) << std::endl;
    }

    std::cout << -f1;
}
