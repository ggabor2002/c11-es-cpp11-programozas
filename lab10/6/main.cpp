#include <functional>
#include <string>
#include <iostream>
#include <cmath>

auto logged_func(const std::function<double(double)> &f, std::string name)
{
    // Miért nem kell kiírni, hogy std:: a move elé?
    return [f, name = move(name)](double x)
    {
        std::cout << "Called: " << name << "(" << x << ")" << std::endl;
        return f(x);
    };
}

int main()
{
    auto logged_sin = logged_func(sin, "sin");
    std::cout << logged_sin(1.2) << std::endl;
}
