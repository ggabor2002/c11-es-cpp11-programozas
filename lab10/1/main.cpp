#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <functional>

double doubler(double d) {
    return d * 2;
}

int main() {
    std::vector<double> v = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    std::transform(v.begin(), v.end(), v.begin(), sqrt);

    // 1
    std::transform(v.begin(), v.end(), v.begin(), doubler);

    // 2
    std::transform(v.begin(), v.end(), v.begin(), [](double d) { return d * 2; });

    // 3
    double megadott = 2;
    std::transform(v.begin(), v.end(), v.begin(), [megadott](double d) { return d * megadott; });

    // 4
    using namespace std::placeholders;
    std::transform(v.begin(), v.end(), v.begin(), std::bind(std::multiplies<double>{}, _1, 2));

    // 5
    std::transform(v.begin(), v.end(), v.begin(), std::bind(std::multiplies<double>{}, _1, megadott));

    for (auto i : v) {
        std::cout << i << std::endl;
    }
}
