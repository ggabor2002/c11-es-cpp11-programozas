#include <string>
#include <stdexcept>
#include <algorithm>

#include "solver.hpp"

template class Solver<int>;
template class Solver<std::string>;

template <typename VALUETYPE>
typename Solver<VALUETYPE>::VariableFunc Solver<VALUETYPE>::add_variable(std::vector<VALUETYPE> values)
{
    if (values.empty())
        throw std::runtime_error("add_variable: a lehetseges ertekek halmaza nem lehet ures");
    for (size_t i = 0; i < values.size() - 1; ++i)
        for (size_t j = i + 1; j < values.size(); ++j)
            if (values[i] == values[j])
                throw std::runtime_error("add_variable: egyforma elemek is vannak a taroloban");
    variables_.push_back(std::move(values));
    size_t pos = variables_.size() - 1;
    return [this, pos]() -> VALUETYPE const &
    {
        if (iterators_.empty())
            throw std::runtime_error("variable_func: csak a solve-on keresztul hivhato a fuggveny");
        return *iterators_[pos];
    };
}

template <typename VALUETYPE>
void Solver<VALUETYPE>::add_constraint(ConstraintFunc constraint)
{
    constraints_.push_back(std::move(constraint));
}

template <typename VALUETYPE>
void Solver<VALUETYPE>::solve(ActivityFunc do_what)
{
    /* iterátorok inicializálása */
    iterators_.clear();
    for (auto &v : variables_)
        iterators_.push_back(v.begin());

    /* A Variable-k current iterátorait már mind begin-re
     * állítottuk, szóval most az elején áll az összes. */
    bool end = false;
    while (!end)
    {
        /* Jó az aktuális állapot? Ha mindegyik igazat ad. */
        bool satisfied = std::all_of(
            constraints_.begin(), constraints_.end(),
            [](const auto &cons)
            { return cons(); });

        /* Ha mindegyik Constraint szerint jó az aktuális megoldás: */
        if (satisfied)
            do_what();

        /* Következőre ugrás: a digit teljes összeadója kódban. */
        bool carry = true;
        for (size_t i = 0; i < variables_.size(); ++i)
        {
            ++iterators_[i];
            carry = iterators_[i] == variables_[i].end();
            if (carry)
                iterators_[i] = variables_[i].begin();
            else
                break;
        }
        /* Ha vége lett, és még mindig carry=true, körbeértünk. */
        end = carry;
    }

    /* ezek már nem kellenek */
    iterators_.clear();
}
