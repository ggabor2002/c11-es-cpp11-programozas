#include <functional>
#include <vector>

template <typename VALUETYPE>
class Solver
{
public:
    /* VariableFunc: ez azt a változót reprezentálja, amely tesztelés
     * közben különféle értékeket vesz fel. Ha meghívjuk a függvényt,
     * megkapjuk az értéket. */
    using VariableFunc = std::function<VALUETYPE const &()>;
    /* ConstraintFunc: egy olyan paraméter nélküli függvény, amely megmondja,
     * hogy az elvárt összefüggések épp teljesülnek-e. */
    using ConstraintFunc = std::function<bool()>;
    /* ActivityFunc: ilyen függvényt fog meghívni a Solver, amikor tesztelés
     * közben talál egy megoldást. */
    using ActivityFunc = std::function<void()>;

private:
    /* Ezeket a változókat kell vizsgálni */
    std::vector<std::vector<VALUETYPE>> variables_;
    /* Ez meg a vizsgálat közben fog kelleni */
    std::vector<typename std::vector<VALUETYPE>::const_iterator> iterators_;
    /* Ezen feltételek szerint fog futni a vizsgálat */
    std::vector<ConstraintFunc> constraints_;

public:
    /* Egy változó lehetséges értékeit tartalmazó tárolót várja.
     * Ad egy olyan függvényt vissza, amelyet meghívva
     * lekérdezhető, hogy a tesztelés közben az adott változónak éppen
     * mi az értéke. A használó ezekre a függvényekre tud majd építeni.
     * Az iterátorok egyelőre nem léteznek, úgyhogy nem tudjuk az iterátor
     * referenciáját eltárolni. */
    VariableFunc add_variable(std::vector<VALUETYPE> values);

    /* Feltétel hozzáadása. A feltétel egy függvény, amely igaz értéke
     * esetén az aktuális állapot elfogadható. */
    void add_constraint(ConstraintFunc constraint);

    /* Teszteli az összes lehetséges értéket, és amikor olyan
     * állapotban vannak az iterátorok, amik épp egy elfogadható
     * kombinációra mutatnak, akkor meghívja a függvényt. */
    void solve(ActivityFunc do_what);
};
