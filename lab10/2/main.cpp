#include <cmath>
#include <iostream>

double heron(double x)
{
    auto good_enough = [x](double guess)
    {
        return fabs(guess * guess - x) < 0.001;
    };
    auto improve = [x](double guess)
    {
        return (guess + x / guess) / 2;
    };
    double guess = 1.0;
    while (!good_enough(guess))
    {
        guess = improve(guess);
    }
    return guess;
}

int main(int, char **)
{
    std::cout << heron(2) << std::endl;
}
