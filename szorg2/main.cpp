#include <string_view>
#include <memory>
#include <exception>
#include <array>
#include <iostream>
#include <queue>

struct Morse final
{
    char character;
    std::string_view sequence;

    explicit constexpr Morse(char character, std::string_view sequence) : character{character}, sequence{sequence} {}
};

struct MorseTree final
{
    char character = '\0';
    std::shared_ptr<MorseTree> dot = nullptr;
    std::shared_ptr<MorseTree> dash = nullptr;

    void add(Morse morse)
    {
        if (morse.sequence.length() == 0)
        {
            if (character == '\0')
            {
                character = morse.character;
            }
            else
            {
                throw std::invalid_argument{"There is already a character in this node."};
            }
        }
        else
        {
            switch (morse.sequence[0])
            {
            case '.':
                if (dot == nullptr)
                {
                    dot = std::make_shared<MorseTree>();
                }
                dot->add(Morse{morse.character, morse.sequence.substr(1)});
                break;
            case '-':
                if (dash == nullptr)
                {
                    dash = std::make_shared<MorseTree>();
                }
                dash->add(Morse{morse.character, morse.sequence.substr(1)});
                break;
            default:
                throw std::invalid_argument{"The sequece can only contain dots and dashes."};
                break;
            }
        }
    }
};

int main()
{
    constexpr std::array<Morse, 36> morse_data{
        Morse{'A', ".-"},
        Morse{'B', "-..."},
        Morse{'C', "-.-."},
        Morse{'D', "-.."},
        Morse{'E', "."},
        Morse{'F', "..-."},
        Morse{'G', "--."},
        Morse{'H', "...."},
        Morse{'I', ".."},
        Morse{'J', ".---"},
        Morse{'K', "-.-"},
        Morse{'L', ".-.."},
        Morse{'M', "--"},
        Morse{'N', "-."},
        Morse{'O', "---"},
        Morse{'P', ".--."},
        Morse{'Q', "--.-"},
        Morse{'R', ".-."},
        Morse{'S', "..."},
        Morse{'T', "-"},
        Morse{'U', "..-"},
        Morse{'V', "...-"},
        Morse{'W', ".--"},
        Morse{'X', "-..-"},
        Morse{'Y', "-.--"},
        Morse{'Z', "--.."},
        Morse{'1', ".----"},
        Morse{'2', "..---"},
        Morse{'3', "...--"},
        Morse{'4', "....-"},
        Morse{'5', "....."},
        Morse{'6', "-...."},
        Morse{'7', "--..."},
        Morse{'8', "---.."},
        Morse{'9', "----."},
        Morse{'0', "-----"},
    };
    auto root = std::make_shared<MorseTree>();
    for (const auto &morse : morse_data)
    {
        root->add(morse);
    }
    std::cout << R"(#include <string_view>
#include <iostream>
#include <exception>
#include <array>

class MorseTree final
{
private:
    char _character;
    MorseTree *_dot;
    MorseTree *_dash;

public:
    explicit constexpr MorseTree(char character, MorseTree *dot, MorseTree *dash) : _character{character}, _dot{dot}, _dash{dash} {}

    void decode(std::string_view sequence) const
    {
        if (sequence.length() == 0)
        {
            std::cout << _character;
        }
        else
        {
            switch (sequence[0])
            {
            case '.':
                if (_dot == nullptr)
                {
                    throw std::domain_error{"Could not decode the squence."};
                }
                _dot->decode(sequence.substr(1));
                break;
            case '-':
                if (_dash == nullptr)
                {
                    throw std::domain_error{"Could not decode the squence."};
                }
                _dash->decode(sequence.substr(1));
                break;
            default:
                throw std::invalid_argument{"The sequece can only contain dots and dashes."};
                break;
            }
        }
    }
};

int main()
{
    std::array<MorseTree, 40> root{
)";
    std::queue<std::shared_ptr<MorseTree>> queue{};
    queue.push(root);
    for (int processed = 0; queue.size() > 0; ++processed)
    {
        auto morseTree = queue.front();
        queue.pop();
        std::cout << "        MorseTree{";
        if (isprint(morseTree->character))
        {
            std::cout << "'" << morseTree->character << "'";
        }
        else
        {
            std::cout << 0;
        }
        std::cout << ", ";
        if (morseTree->dot != nullptr)
        {
            queue.push(morseTree->dot);
            std::cout << "&root[" << processed + queue.size() << "]";
        }
        else
        {
            std::cout << "nullptr";
        }
        std::cout << ", ";
        if (morseTree->dash != nullptr)
        {
            queue.push(morseTree->dash);
            std::cout << "&root[" << processed + queue.size() << "]";
        }
        else
        {
            std::cout << "nullptr";
        }
        std::cout << "}," << std::endl;
    }
    std::cout << R"(    };
    std::array<std::string_view, 7> secret{
        "--..",
        "...",
        "---",
        "--",
        "-...",
        "---",
        ".-.",
    };
    for (auto letter : secret)
    {
        root[0].decode(letter);
    }
    std::cout << std::endl;
    return 0;
}
)";
    return 0;
}
