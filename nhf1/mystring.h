#pragma once

#include <iostream>
#include <vector>

class MyString final
{
    class Proxy final
    {
    private:
        MyString &str_;
        std::size_t index_;

    public:
        explicit Proxy(MyString &str, std::size_t index);

        Proxy(const Proxy &) = delete;
        Proxy(Proxy &&) noexcept = delete;

        char operator=(char c);
        char operator=(const Proxy &);

        operator char() const;
    };

    class StringValue final
    {
        friend MyString;
        friend Proxy;

    private:
        // Only for debugging.
        static long instance_count;

    public:
        // Only for debugging.
        static long get_instance_count();

    private:
        long use_count_ = 1;
        char *c_str_;

        explicit StringValue(char c);
        explicit StringValue(const char *c_str = "");
        explicit StringValue(char *&&c_str) noexcept;

        StringValue(const StringValue &) = delete;
        StringValue(StringValue &&) noexcept = delete;
        StringValue &operator=(const StringValue &) = delete;
        StringValue &operator=(StringValue &&) noexcept = delete;

        ~StringValue();

        friend std::ostream &operator<<(std::ostream &os, const MyString &str);

        std::size_t length() const;
    };

private:
    static std::vector<StringValue *> stringValues_;

    static StringValue *stringValueFrom(char c);
    static StringValue *stringValueFrom(const char *c_str);
    static StringValue *stringValueFrom(char *&&c_str);

public:
    static long get_StringValue_instance_count();

private:
    StringValue *stringValue_;

    inline void release_stringValue();
    inline void detach();

    explicit MyString(StringValue *&&stringValue) noexcept;

public:
    explicit MyString(char c);
    explicit MyString(const char *c_str = "");
    MyString(const MyString &other);
    MyString(MyString &&other) noexcept;

    MyString &operator=(char c);
    MyString &operator=(const char *c_str);
    MyString &operator=(const MyString &other);
    MyString &operator=(MyString &&other) noexcept;

    ~MyString();

    MyString operator+(char c) const;
    MyString operator+(const char *c_str) const;
    MyString operator+(const MyString &other) const;

    MyString &operator+=(char c);
    MyString &operator+=(const char *c_str);
    MyString &operator+=(const MyString &other);

    friend std::ostream &operator<<(std::ostream &os, const MyString &str);
    friend std::istream &operator>>(std::istream &is, MyString &str);

    std::size_t length() const;
    void printStringValueAddress(std::ostream &os) const;

    Proxy operator[](std::size_t index);
    const char &operator[](std::size_t index) const;
};
