#include <iostream>
#include "mystring.h"
#include <string>

inline void test()
{
    auto x = MyString{"hello"};
    auto y = MyString{x};
    y[0] = 'c';
    std::cout << x << std::endl; /* hello */
    std::cout << y << std::endl; /* cello */

    auto a = MyString{"hello"};
    auto b = MyString{a};
    std::cout << b[1] << std::endl; /* e */
    a[1] = 'a';
    b[1] = b[4];
    std::cout << a << " " << b << std::endl; /* hallo hollo */

    auto c = MyString{"helló világ"};
    auto d = MyString{"helló világ"};
    c.printStringValueAddress(std::cout);
    d.printStringValueAddress(std::cout);
}

int main()
{
    std::cout << "################################################################" << std::endl;
    test();
    std::cout << "################################################################" << std::endl;

    auto success = MyString::get_StringValue_instance_count() == 0;
    std::cout << "Success: " << std::boolalpha << success << std::endl;
    return 0;
}
