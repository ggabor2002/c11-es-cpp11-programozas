#pragma once

#include <cstdint>
#include <vector>

template <class Derived, typename VertexData, typename EdgeData>
class Graph
{
protected:
    constexpr Graph() = default;

public:
    using Id = std::uint64_t;

    [[nodiscard]] constexpr Id add_vertex(VertexData data) {
        return static_cast<Derived*>(this)->add_vertex_impl(data);
    }
    [[nodiscard]] constexpr VertexData &get_vertex_data(Id id) {
        return static_cast<Derived*>(this)->get_vertex_data_impl(id);
    }
    [[nodiscard]] constexpr std::vector<Id> get_vertex_edges(Id from) {
        return static_cast<Derived*>(this)->get_vertex_edges_impl(from);
    }
    constexpr void add_edge(EdgeData data, Id from, Id to) {
        static_cast<Derived*>(this)->add_edge_impl(data, from, to);
    }
    [[nodiscard]] constexpr EdgeData &get_edge_data(Id from, Id to) {
        return static_cast<Derived*>(this)->get_edge_data_impl(from, to);
    }
};
