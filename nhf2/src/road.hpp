#pragma once

class Road final
{
private:
    double length_;

public:
    [[nodiscard]] constexpr explicit Road(double length) : length_{ length } {}

    [[nodiscard]] constexpr double get_length() {
        return length_;
    }
};
