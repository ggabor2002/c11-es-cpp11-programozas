#pragma once

#include <functional>
#include <set>

#include "graph.hpp"

template <typename VertexData, typename EdgeData, typename GraphT, typename Func>
    requires(std::is_base_of_v<Graph<GraphT, VertexData, EdgeData>, GraphT>&& std::is_invocable_r_v<void, std::remove_cvref_t<Func>(VertexData), VertexData>)
constexpr void depthFirstSearch(
    GraphT& graph,
    typename Graph<GraphT, VertexData, EdgeData>::Id from,
    Func function)
{
    auto visited = std::set<decltype(from)>{};

    std::function<void(decltype(from))> depthFirstSearch_ = [&](decltype(from) id)
    {
        visited.insert(id);
        function(graph.get_vertex_data(id));
        for (auto edge : graph.get_vertex_edges(id))
        {
            if (!visited.contains(edge))
            {
                depthFirstSearch_(edge);
            }
        }
    };

    depthFirstSearch_(from);
}
