#pragma once

#include <string>
#include <string_view>

class Location final
{
private:
    std::string name_;

public:
    [[nodiscard]] constexpr explicit Location(std::string name) : name_{ std::move(name) } {}

    [[nodiscard]] constexpr std::string_view get_name() {
        return std::string_view{ name_ };
    }
};
