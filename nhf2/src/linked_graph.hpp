#pragma once

#include <vector>
#include <memory>
#include <ranges>

#include "graph.hpp"

template <typename VertexData, typename EdgeData>
class LinkedGraph final : public Graph<LinkedGraph<VertexData, EdgeData>, VertexData, EdgeData>
{
    friend Graph<LinkedGraph, VertexData, EdgeData>;
private:
    using Id = Graph<LinkedGraph, VertexData, EdgeData>::Id;

    struct Edge;

    struct Vertex final
    {
        VertexData data_;
        Id id_;
        std::vector<std::shared_ptr<Edge>> edges_{};

        [[nodiscard]] constexpr explicit Vertex(VertexData data, Id id)
            : data_{std::move(data)},
              id_{id}
        {
        }
    };

    struct Edge final
    {
        EdgeData data_;
        std::weak_ptr<Vertex> from_;
        std::weak_ptr<Vertex> to_;

        [[nodiscard]] constexpr explicit Edge(EdgeData data, std::shared_ptr<Vertex> from, std::shared_ptr<Vertex> to)
            : data_{std::move(data)},
              from_{from},
              to_{to}
        {
        }
    };

    std::vector<std::shared_ptr<Vertex>> vertices_{};

    [[nodiscard]] constexpr Id add_vertex_impl(VertexData data)
    {
        Id id = vertices_.size();
        auto vertex = std::shared_ptr<Vertex>{new Vertex{std::move(data), id}};
        vertices_.push_back(vertex);
        return id;
    }

    [[nodiscard]] constexpr VertexData &get_vertex_data_impl(Id id)
    {
        return vertices_[id]->data_;
    }

    [[nodiscard]] constexpr std::vector<Id> get_vertex_edges_impl(Id from)
    {
        // If GCC would support C++23.
        /*
        return vertices_[from]->edges_
            | std::views::transform([](auto edge) { return edge->to_.lock()->id_; });
            | std::ranges::to<std::vector>();
        */

        // For now...
        auto result = vertices_[from]->edges_
            | std::views::transform([](auto edge) { return edge->to_.lock()->id_; });
        return std::vector<Id>{result.begin(), result.end()};
    }

    constexpr void add_edge_impl(EdgeData data, Id from, Id to)
    {
        auto edge = std::shared_ptr<Edge>{new Edge{std::move(data), vertices_[from], vertices_[to]}};
        vertices_[from]->edges_.push_back(edge);
    }

    [[nodiscard]] constexpr EdgeData &get_edge_data_impl(Id from, Id to)
    {
        auto result = std::ranges::find_if(vertices_[from]->edges_, [to](auto edge) { return edge->to_.lock()->id_ == to; });
        return result[0]->data_;
    }
};
