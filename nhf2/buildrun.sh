#!/bin/bash

clear
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++ -Bbuild && \
cmake --build build --config Debug --target nhf2 -- && \
build/nhf2
