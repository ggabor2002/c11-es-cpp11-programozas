cmake_minimum_required(VERSION 3.1.3)
project(nhf2 VERSION 0.1.0)

add_executable(nhf2)

set_target_properties(nhf2 PROPERTIES
    CXX_STANDARD 23
    CXX_STANDARD_REQUIRED ON
)

target_compile_options(nhf2 PRIVATE
    -Werror
    -Wall
    -Wextra
)

target_sources(nhf2 PRIVATE
    src/main.cpp
    src/location.cpp
    src/road.cpp
)
