#include <memory>
#include <iostream>

class Expression
{
public:
    /* returns value of expression at x */
    virtual double evaluate(double x) const = 0;

    /* outputs expression as string to stream os */
    virtual void print(std::ostream &os) const = 0;

    /* return dynamically allocated derivative expression */
    virtual std::shared_ptr<Expression> derivative() const = 0;

    Expression() = default;
    Expression(Expression const &) = default;

    virtual ~Expression() = default;
};

/* pass print request to virtual print() method */
std::ostream &operator<<(std::ostream &os, Expression const &e)
{
    e.print(os);
    return os;
}

class Constant final : public Expression
{
private:
    double c_;

public:
    explicit Constant(double c) : c_{c} {}

    double evaluate(double) const override
    {
        return c_;
    }

    void print(std::ostream &os) const override
    {
        os << c_;
    }

    std::shared_ptr<Expression> derivative() const override
    {
        return std::make_shared<Constant>(0);
    }

    double get_value() const
    {
        return c_;
    }
};

class Variable final : public Expression
{
public:
    double evaluate(double x) const override
    {
        return x;
    }

    void print(std::ostream &os) const override
    {
        os << 'x';
    }

    std::shared_ptr<Expression> derivative() const override
    {
        return std::make_shared<Constant>(1);
    }
};

class TwoOperand : public Expression
{
public:
    /* create object, adopt dynamically allocated expressions */
    TwoOperand(std::shared_ptr<Expression> lhs, std::shared_ptr<Expression> rhs) : lhs_{lhs}, rhs_{rhs} {}

    /* copy constructor */
    TwoOperand(TwoOperand const &the_other) = default;

    /* no copy assignment */
    TwoOperand &operator=(TwoOperand const &) = delete;

    double evaluate(double x) const final
    {
        return do_operator(lhs_->evaluate(x), rhs_->evaluate(x));
    }

    void print(std::ostream &os) const final
    {
        os << '(' << *lhs_ << get_operator() << *rhs_ << ')';
    }

private:
    /* subclass has to provide function to return its operator */
    virtual char get_operator() const = 0;

    /* subclass has to provide function to do the calculation */
    virtual double do_operator(double lhs, double rhs) const = 0;

protected:
    /* left and right hand side operands */
    std::shared_ptr<Expression> lhs_;
    std::shared_ptr<Expression> rhs_;
};

class Sum final : public TwoOperand
{
public:
    using TwoOperand::TwoOperand;

    std::shared_ptr<Expression> derivative() const override
    {
        return std::make_shared<Sum>(lhs_->derivative(), rhs_->derivative());
    }

private:
    char get_operator() const override
    {
        return '+';
    }

    double do_operator(double lhs, double rhs) const override
    {
        return lhs + rhs;
    }
};

class Product final : public TwoOperand
{
public:
    using TwoOperand::TwoOperand;

    std::shared_ptr<Expression> derivative() const override
    {
        return std::make_shared<Sum>(
            std::make_shared<Product>(lhs_->derivative(), rhs_),
            std::make_shared<Product>(lhs_, rhs_->derivative()));
    }

private:
    char get_operator() const override
    {
        return '*';
    }

    double do_operator(double lhs, double rhs) const override
    {
        return lhs * rhs;
    }
};

int main()
{
    std::shared_ptr<Expression> c = std::make_shared<Product>(
        std::make_shared<Constant>(),
        std::make_shared<Sum>(
            std::make_shared<Constant>(3),
            std::make_shared<Variable>()));

    std::cout << "f(x) = " << *c << std::endl;
    std::cout << "f(3) = " << c->evaluate(3) << std::endl;

    std::shared_ptr<Expression> cd = c->derivative();
    std::cout << "f'(x) = " << *cd << std::endl;
}
