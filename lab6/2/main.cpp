#include <iostream>
#include <iomanip>
#include <cmath>

template <typename F>
class Derival final
{
private:
    F f;
    double dx = 0.001;

public:
    explicit Derival(F f) : f{f} {}
    double operator()(double x) const
    {
        return (f(x + dx) - f(x)) / dx;
    }
};

class Parabola final
{
private:
    double a, b, c;

public:
    Parabola(double a, double b, double c) : a{a}, b{b}, c{c} {}
    double operator()(double x) const
    {
        return a * x * x + b * x + c;
    }
};

int main()
{
    auto my_cos = Derival{sin};
    for (double f = 0; f < 3.1415; f += 0.1)
    {
        std::cout << std::setw(20) << my_cos(f) - cos(f) << std::endl;
    }

    Parabola p1{0.5, 2.3, -5}; /* 0.5x^2 + 2.3x - 5 */
    for (double f = 0; f < 3.0; f += 0.1)
    {
        std::cout << std::setw(20) << p1(f) << std::endl;
    }

    auto p1_der = Derival{p1};
    Parabola p2{0, 1, 2.3}; /* x + 2.3, p1 deriváltja */
    for (double f = 0; f < 3.0; f += 0.1)
    {
        std::cout << std::setw(20) << p1_der(f) - p2(f) << std::endl;
    }
}
