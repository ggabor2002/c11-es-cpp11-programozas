cmake_minimum_required(VERSION 3.0.0)
project(szorg12 VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 17)

add_executable(szorg12 main.cpp)

target_compile_options(szorg12 PRIVATE -Wall -Wextra)
