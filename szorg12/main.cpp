#include <functional>
#include <iostream>
#include <cmath>

template <typename Res, typename... Args>
struct MukodjeMar
{
};

template <typename Valami>
class my_function;

template <typename Res, typename... Args>
class my_function<Res(Args...)>
    : public MukodjeMar<Res, Args...>
{
};

int main()
{
    std::function<double(double)> f;

    if (!f)
        std::cout << "Egyelőre nullptr" << std::endl;

    f = sin;
    std::cout << sin(2.3) << "==" << f(2.3) << std::endl;

    f = [](double x)
    { return x * x; };
    std::cout << 2.3 * 2.3 << "==" << f(2.3) << std::endl;

    f = std::bind(pow, std::placeholders::_1, 4);
    std::cout << pow(2.3, 4) << "==" << f(2.3) << std::endl;

    auto f2 = f; /* másolható */
    std::cout << pow(2.3, 4) << "==" << f2(2.3) << std::endl;

    f = nullptr;
    try
    {
        f(2.3);
    }
    catch (std::bad_function_call &e)
    {
        std::cout << "Megint nullptr" << std::endl;
    }
}
