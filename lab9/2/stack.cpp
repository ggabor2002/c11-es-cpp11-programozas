#include <iostream>
#include <cstdlib>
#include <new>
#include <stdexcept>
#include <string>

template <typename T>
class Stack
{
public:
    explicit Stack(size_t max_size);
    Stack(Stack const &orig);
    Stack &operator=(Stack const &orig) = delete; // Ennél kicsit tanácstalan vagyok. A swap függvény kéne, hogy noexcept lehessen?
    ~Stack();
    void push(T const &what);
    T pop() noexcept(noexcept(T{pData_[size_]}));
    bool empty() const noexcept;

private:
    size_t size_;
    size_t max_size_;
    T *pData_;
};

// Kivételdobás ha nem sikerült memóriát foglalni.
template <typename T>
Stack<T>::Stack(size_t max_size)
{
    size_ = 0;
    max_size_ = max_size;
    pData_ = (T *)malloc(sizeof(T) * max_size_);
    if (pData_ == nullptr)
    {
        throw std::bad_alloc{};
    }
}

// Konstruktordelegálás, hogy csak egy helyen dobáljunk.
template <typename T>
Stack<T>::Stack(Stack<T> const &orig) : Stack{orig.max_size_}
{
    size_ = orig.size_;
    for (size_t i = 0; i != size_; ++i)
    {
        new (&pData_[i]) T{orig.pData_[i]};
    }
}

// A destruktor alapból noexcept, nem kell csinálni semmit.
template <typename T>
Stack<T>::~Stack()
{
    for (size_t i = 0; i != size_; ++i)
    {
        pData_[i].~T();
    }
    free(pData_);
}

// Kivételt kell dobni ha tele van a tároló
template <typename T>
void Stack<T>::push(T const &what)
{
    if (size_ == max_size_)
    {
        throw std::out_of_range{"Stack is full"};
    }
    size_++;
    new (&pData_[size_ - 1]) T{what};
}

// Ez is simán dobálhatná a kivételeket ha üres.
// De akkor nem lenne kemény dió, úgyhogy gondolom nem erre gondolt a költő.
// Akkor noexcept, ha a T másoló konstruktora is az.
template <typename T>
T Stack<T>::pop() noexcept(noexcept(T{pData_[size_]}))
{
    size_--;
    T saved{std::move(pData_[size_])};
    pData_[size_].~T();
    return saved;
}

// Egyértelműen noexcept
template <typename T>
bool Stack<T>::empty() const noexcept
{
    return size_ == 0;
}

int main()
{
    std::cout << "Irj be szavakat fajl vege jelig!" << std::endl;
    Stack<std::string> s(10);
    std::string word;
    while (std::cin >> word)
    {
        try
        {
            s.push(word);
        }
        catch (const std::exception &e)
        {
            std::cout << e.what() << std::endl;
            break;
        }
    }
    while (!s.empty())
    {
        std::cout << s.pop() << std::endl;
    }
}
