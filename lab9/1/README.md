```C++
String EvaluateSalaryAndReturnName(Employee e) {
    if (e.Title() == "CEO" || e.Salary() > 100000) {
        cout << e.First() << " " << e.Last()
             << " is overpaid" << endl;
    }
    return e.First() + " " + e.Last();
}
```

- Az  `if`-ben lehet, hogy meg sem hívódik a `Salary` függvény.
- A `Title`, `Salary`. `First` és `Last` függvények bármelyike dobhat kivételt.
- Az `==`, `>`, `<<` és `+` operátorok bármelyike is dobhat kivételt.
- Visszatéréskor a másoló konsturktor dobhat kivétélet. Mármint ha régi C++-al van fordítva és nem jó helyre hozódik létre alapból.
