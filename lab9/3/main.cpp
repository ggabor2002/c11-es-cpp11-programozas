#include <utility>

template <typename T>
class Node
{
private:
    T data;
    Node *left = nullptr;
    Node *right = nullptr;

public:
    Node(Node const &original) : data(original.data)
    {
        if (original.left != nullptr)
        {
            left = new Node(*original.left);
        }
        if (original.right != nullptr)
        {
            right = new Node(*original.right);
        }
    }

    Node(Node &&original) noexcept(noexcept(data(std::move(original.data))))
        : data(std::move(original.data)),
          left(original.left),
          right(original.right)
    {
        original.left = nullptr;
        original.right = nullptr;
    }

    ~Node()
    {
        delete left;
        delete right;
    }

    void swap(Node const &original) noexcept(noexcept(std::swap(data, original.data)))
    {
        std::swap(data, original.data);
        std::swap(left, original.left);
        std::swap(right, original.right);
    }

    Node &operator=(Node original) noexcept(noexcept(swap(original)))
    {
        swap(original);
        return *this;
    }
};
